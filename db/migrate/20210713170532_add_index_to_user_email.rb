# frozen_string_literal: true

# Add uniqueness to email by making it an index
class AddIndexToUserEmail < ActiveRecord::Migration[6.1]
  def change
    add_index :users, :email, unique: true
  end
end
