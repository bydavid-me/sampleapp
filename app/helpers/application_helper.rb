module ApplicationHelper
  # Return full title on every page.
  def full_title(page_title = '')
    base_title = "byDavid.me's Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end
end
